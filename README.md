## Linux

  1. Install Python 3.x using your package manager
  1. In a terminal, run the following commands from the top level directory of your local copy of this repository:
    1. python3 -m venv venv
	1. source venv\bin\activate
	1. pip install -r requirements
	1. pip install geopandas
  1. Run the following command to launch Jupyter: jupyter notebook
	
That should do it.

## Windows

  1. Download and install [Python 3.9](https://www.python.org/downloads/release/python-3910/)  (Note: the version matters!)
  1. Download the following "wheel" files from Christoph Gohlke's site.  Save them using the indicated file names.
    1. [pyproj](https://www.lfd.uci.edu/~gohlke/pythonlibs/#pyproj)
	  - Download: pyproj-3.3.0-cp39-cp39-win_amd64.whl
	  - Save as: pyproj.whl
	1. [rtree](https://www.lfd.uci.edu/~gohlke/pythonlibs/#rtree)
	  - Download: Rtree-0.9.7-cp39-cp39-win_amd64.whl
	  - Save as: rtree.whl
	1. [Shapely](https://www.lfd.uci.edu/~gohlke/pythonlibs/#shapely)
	  - Download: Shapely-1.8.0-cp39-cp39-win_amd64.whl
	  - Save as: shapely.whl
	1. [GDAL](https://www.lfd.uci.edu/~gohlke/pythonlibs/#gdal)
	  - Download: GDAL-3.4.1-cp39-cp39-win_amd64.whl
	  - Save as: gdal.whl
	1. [Fiona](https://www.lfd.uci.edu/~gohlke/pythonlibs/#fiona)
	  - Download: Fiona-1.8.20-cp39-cp39-win_amd64.whl
	  - Save as: fiona.whl
  1. In a terminal (command prompt), run the following commands from the top level directory of your local copy of this repository:
    1. python -m venv venv
	1. venv\Scripts\activate
	1. pip install -r requirements
	1. install_geopandas.bat
  1. Run the following command to launch Jupyter: jupyter notebook
